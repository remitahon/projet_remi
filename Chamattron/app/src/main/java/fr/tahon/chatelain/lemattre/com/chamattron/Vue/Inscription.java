package fr.tahon.chatelain.lemattre.com.chamattron.Vue;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import fr.tahon.chatelain.lemattre.com.chamattron.R;

public class Inscription extends AppCompatActivity {

    private EditText pseudo;
    private EditText mail;
    private EditText mdp1;
    private EditText mdp2;
    private Button valider;
    private ImageView res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        this.pseudo = findViewById(R.id.Pseudo);
        this.mail = findViewById(R.id.mail);
        this.mdp1 = findViewById(R.id.pass1);
        this.mdp2 = findViewById(R.id.pass2);
        this.valider = findViewById(R.id.valider);
        this.res = findViewById(R.id.resultat);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Verification();
            }
        });
    }

    public void Verification() {
        String lpseudo;
        String lmail;
        String lmdp1;
        String lmdp2;

        lpseudo = this.pseudo.getText().toString();
        lmail = this.mail.getText().toString();
        lmdp1 = this.mdp1.getText().toString();
        lmdp2 = this.mdp2.getText().toString();

        if (!lpseudo.isEmpty() && !lmail.isEmpty() && !lmdp1.isEmpty() && !lmdp2.isEmpty()) {
            if (lmdp1.equals(lmdp2)) {
                this.res.setImageResource(R.drawable.oui);
            } else {
                this.res.setImageResource(R.drawable.non);
            }
        } else {
            this.res.setImageResource(R.drawable.non);
        }
    }
}
