package fr.tahon.chatelain.lemattre.com.chamattron;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.tahon.chatelain.lemattre.com.chamattron.Vue.About;
import fr.tahon.chatelain.lemattre.com.chamattron.Vue.Inscription;

public class MainActivity extends AppCompatActivity {

    private EditText id;
    private EditText Mdp;
    private Button connexion;
    private  Button inscription;
    private Button Apropos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.id = findViewById(R.id.Id);
        this.Mdp = findViewById(R.id.Pass);
        this.connexion = findViewById(R.id.Connect);
        this.inscription = findViewById(R.id.subscribe);
        this.Apropos = findViewById(R.id.about);

        Apropos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Acabout();
            }
        });
        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Acinsciption();
            }
        });

    }

    private void Acabout(){
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

    private void Acinsciption(){
        Intent intent = new Intent(this, Inscription.class);
        startActivity(intent);
    }


}
